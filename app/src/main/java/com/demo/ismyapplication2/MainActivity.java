package com.demo.ismyapplication2;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.ismyapplication2.Utils.UtilTool;
import com.demo.ismyapplication2.Utils.UtilsLog;
import com.demo.ismyapplication2.adapter.ShopcatAdapter;
import com.demo.ismyapplication2.entity.GoodsInfo;
import com.demo.ismyapplication2.entity.ShopCarEntity;
import com.demo.ismyapplication2.entity.StoreInfo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ShopcatAdapter.CheckInterface, ShopcatAdapter.ModifyCountInterface{
    @BindView(R.id.listView)
    ExpandableListView listView;
    @BindView(R.id.all_checkBox)
    CheckBox allCheckBox;
    @BindView(R.id.total_price)
    TextView totalPrice;
    @BindView(R.id.go_pay)
    TextView goPay;
    @BindView(R.id.order_info)
    LinearLayout orderInfo;
    @BindView(R.id.del_goods)
    TextView delGoods;
    @BindView(R.id.ll_cart)
    LinearLayout llCart;

    private Context mcontext;
    private double mtotalPrice = 0.00;
    private int mtotalCount = 0;
    //false就是编辑，ture就是完成
    private boolean flag = false;
    private ShopcatAdapter adapter;
    private List<StoreInfo> groups; //组元素的列表
    private Map<String, List<GoodsInfo>> childs; //子元素的列表

    String json;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initJsonData();
//        initData();
        initEvents();
    }

    private void initJsonData() {


        mcontext = this;
        json=dataJsonStr();
        ShopCarEntity shopCarEntity = new Gson().fromJson(json, ShopCarEntity.class);
        Log.d("数据","     "+shopCarEntity.getData().size());

        groups = new ArrayList<StoreInfo>();
        childs = new HashMap<String, List<GoodsInfo>>();
        List<ShopCarEntity.DataBean> shopCarShopData = shopCarEntity.getData();
        for (int i=0;i<shopCarShopData.size();i++){
            StoreInfo storeInfo=new StoreInfo();
            storeInfo.setId(i+"");
            storeInfo.setName(shopCarShopData.get(i).getShopName());
            groups.add(storeInfo);
            List<GoodsInfo> goods = new ArrayList<>();
            for (int j=0;j<shopCarShopData.get(i).getGoodsOrderList().size();j++){
                GoodsInfo goodsInfo=new GoodsInfo();
                goodsInfo.setId(i + "-" + j);//i-j 就是商品的id， 对应着第几个店铺的第几个商品，1-1 就是第一个店铺的第一个商品
                goodsInfo.setCount(shopCarShopData.get(i).getGoodsOrderList().get(j).getGoodsNum());
                goodsInfo.setName(shopCarShopData.get(i).getGoodsOrderList().get(j).getGoodsName());
                goodsInfo.setPrice(shopCarShopData.get(i).getGoodsOrderList().get(j).getGoodsAmount());
                goodsInfo.setGoodsImageUrl(shopCarShopData.get(i).getGoodsOrderList().get(j).getGoodsImg());
                goodsInfo.setSize("马菊兰 43*48*75CM  37码");
                goods.add(goodsInfo);
            }

            childs.put(groups.get(i).getId(),goods);
        }
    }

    /**
     * 模拟数据<br>
     * 遵循适配器的数据列表填充原则，组元素被放在一个list中，对应着组元素的下辖子元素被放在Map中
     * 其Key是组元素的Id
     */
    private void initData() {
        mcontext = this;
        groups = new ArrayList<StoreInfo>();
        childs = new HashMap<String, List<GoodsInfo>>();
        for (int i = 0; i < 5; i++) {
            groups.add(new StoreInfo(i + "", "第" + (i + 1) + "号店铺"));
            List<GoodsInfo> goods = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                int[] img = {R.drawable.cmaz, R.drawable.cmaz, R.drawable.cmaz, R.drawable.cmaz, R.drawable.cmaz, R.drawable.cmaz};
                //i-j 就是商品的id， 对应着第几个店铺的第几个商品，1-1 就是第一个店铺的第一个商品
                goods.add(new GoodsInfo(i + "-" + j, "商品", groups.get(i).getName() + "的第" + (j + 1) + "个商品", 255.00 + new Random().nextInt(1500), 1555 + new Random().nextInt(3000), "第一排", "出头天者", img[j], new Random().nextInt(100)));
            }
            childs.put(groups.get(i).getId(), goods);
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        setCartNum();
    }

    /**
     * 设置购物车的数量
     */
    private void setCartNum() {
        int count = 0;
        for (int i = 0; i < groups.size(); i++) {
            StoreInfo group = groups.get(i);
            group.setChoosed(allCheckBox.isChecked());
            List<GoodsInfo> Childs = childs.get(group.getId());
            for (GoodsInfo childs : Childs) {
                count++;
            }
        }

        //购物车已经清空
        if (count == 0) {
            clearCart();
        }

    }

    private void clearCart() {
        llCart.setVisibility(View.GONE);
    }

    private void initEvents() {
        adapter = new ShopcatAdapter(groups, childs, mcontext);
        adapter.setCheckInterface(this);//关键步骤1：设置复选框的接口
        adapter.setModifyCountInterface(this); //关键步骤2:设置增删减的接口
        listView.setGroupIndicator(null); //设置属性 GroupIndicator 去掉向下箭头
        listView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            listView.expandGroup(i); //关键步骤4:初始化，将ExpandableListView以展开的方式显示
        }
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int firstVisiablePostion=view.getFirstVisiblePosition();
                int top=-1;
                View firstView=view.getChildAt(firstVisibleItem);
                UtilsLog.i("childCount="+view.getChildCount());//返回的是显示层面上的所包含的子view的个数
                if(firstView!=null){
                    top=firstView.getTop();
                }
                UtilsLog.i("firstVisiableItem="+firstVisibleItem+",fistVisiablePosition="+firstVisiablePostion+",firstView="+firstView+",top="+top);
            }
        });
    }



    /**
     * 删除操作
     * 1.不要边遍历边删除,容易出现数组越界的情况
     * 2.把将要删除的对象放进相应的容器中，待遍历完，用removeAll的方式进行删除
     */
    private void doDelete() {
        List<StoreInfo> toBeDeleteGroups = new ArrayList<StoreInfo>(); //待删除的组元素
        for (int i = 0; i < groups.size(); i++) {
            StoreInfo group = groups.get(i);
            if (group.isChoosed()) {
                toBeDeleteGroups.add(group);
            }
            List<GoodsInfo> toBeDeleteChilds = new ArrayList<GoodsInfo>();//待删除的子元素
            List<GoodsInfo> child = childs.get(group.getId());
            for (int j = 0; j < child.size(); j++) {
                if (child.get(j).isChoosed()) {
                    toBeDeleteChilds.add(child.get(j));
                }
            }
            child.removeAll(toBeDeleteChilds);
        }
        groups.removeAll(toBeDeleteGroups);
        //重新设置购物车
        setCartNum();
        calulate();//删除也要计算合计  以及支付数量
        adapter.notifyDataSetChanged();

    }

    /**
     * @param groupPosition 组元素的位置
     * @param isChecked     组元素的选中与否
     *                      思路:组元素被选中了，那么下辖全部的子元素也被选中
     */
    @Override
    public void checkGroup(int groupPosition, boolean isChecked) {
        StoreInfo group = groups.get(groupPosition);
        List<GoodsInfo> child = childs.get(group.getId());
        for (int i = 0; i < child.size(); i++) {
            child.get(i).setChoosed(isChecked);
        }
        if (isCheckAll()) {
            allCheckBox.setChecked(true);//全选
        } else {
            allCheckBox.setChecked(false);//反选
        }
        adapter.notifyDataSetChanged();
        calulate();
    }

    /**
     * @return 判断组元素是否全选
     */
    private boolean isCheckAll() {
        for (StoreInfo group : groups) {
            if (!group.isChoosed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param groupPosition 组元素的位置
     * @param childPosition 子元素的位置
     * @param isChecked     子元素的选中与否
     */
    @Override
    public void checkChild(int groupPosition, int childPosition, boolean isChecked) {
        boolean allChildSameState = true; //判断该组下面的所有子元素是否处于同一状态
        StoreInfo group = groups.get(groupPosition);
        List<GoodsInfo> child = childs.get(group.getId());
        for (int i = 0; i < child.size(); i++) {
            //不选全中
            if (child.get(i).isChoosed() != isChecked) {
                allChildSameState = false;
                break;
            }
        }

        if (allChildSameState) {
            group.setChoosed(isChecked);//如果子元素状态相同，那么对应的组元素也设置成这一种的同一状态
        } else {
            group.setChoosed(false);//否则一律视为未选中
        }

        if (isCheckAll()) {
            allCheckBox.setChecked(true);//全选
        } else {
            allCheckBox.setChecked(false);//反选
        }

        adapter.notifyDataSetChanged();
        calulate();

    }

    @Override
    public void doIncrease(int groupPosition, int childPosition, View showCountView, boolean isChecked) {
        GoodsInfo good = (GoodsInfo) adapter.getChild(groupPosition, childPosition);
        int count = good.getCount();
        count++;
        good.setCount(count);
        ((TextView) showCountView).setText(String.valueOf(count));
        adapter.notifyDataSetChanged();
        calulate();
    }

    /**
     * @param groupPosition
     * @param childPosition
     * @param showCountView
     * @param isChecked
     */
    @Override
    public void doDecrease(int groupPosition, int childPosition, View showCountView, boolean isChecked) {
        GoodsInfo good = (GoodsInfo) adapter.getChild(groupPosition, childPosition);
        int count = good.getCount();
        if (count == 1) {
            return;
        }
        count--;
        good.setCount(count);
        ((TextView) showCountView).setText("" + count);
        adapter.notifyDataSetChanged();
        calulate();
    }

    /**
     * @param groupPosition
     * @param childPosition 思路:当子元素=0，那么组元素也要删除
     */
    @Override
    public void childDelete(int groupPosition, int childPosition) {
        StoreInfo group = groups.get(groupPosition);
        List<GoodsInfo> child = childs.get(group.getId());
        child.remove(childPosition);
        if (child.size() == 0) {
            groups.remove(groupPosition);
        }
        adapter.notifyDataSetChanged();
        calulate();


    }

    //手输入数量后更新数据
    public void doUpdate(int groupPosition, int childPosition, View showCountView, boolean isChecked) {
        GoodsInfo good = (GoodsInfo) adapter.getChild(groupPosition, childPosition);
        int count = good.getCount();
        UtilsLog.i("进行更新数据，数量" + count + "");
        ((TextView) showCountView).setText(String.valueOf(count));
        adapter.notifyDataSetChanged();
        calulate();


    }

    @OnClick({R.id.all_checkBox, R.id.go_pay, R.id.del_goods})
    public void onClick(View view) {
        AlertDialog dialog;
        switch (view.getId()) {
            case R.id.all_checkBox:
                doCheckAll();
                break;
            case R.id.go_pay:
                if (mtotalCount == 0) {
                    UtilTool.toast(mcontext, "请选择要支付的商品");
                    return;
                }
                dialog = new AlertDialog.Builder(mcontext).create();
                dialog.setMessage("总计:" + mtotalCount + "种商品，" + mtotalPrice + "元");
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "支付", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                dialog.show();
                break;
            case R.id.del_goods:
                if (mtotalCount == 0) {
                    UtilTool.toast(mcontext, "请选择要删除的商品");
                    return;
                }
                dialog = new AlertDialog.Builder(mcontext).create();
                dialog.setMessage("确认要删除该商品吗?");
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doDelete();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                dialog.show();
                break;
        }
    }


    /**
     * 全选和反选
     * 注意一下问题
     */
    private void doCheckAll() {
        for (int i = 0; i < groups.size(); i++) {
            StoreInfo group = groups.get(i);
            group.setChoosed(allCheckBox.isChecked());
            List<GoodsInfo> child = childs.get(group.getId());
            for (int j = 0; j < child.size(); j++) {
                child.get(j).setChoosed(allCheckBox.isChecked());//这里出现过错误
            }
        }
        adapter.notifyDataSetChanged();
        calulate();
    }

    /**
     * 计算商品总价格，操作步骤
     * 1.先清空全局计价,计数
     * 2.遍历所有的子元素，只要是被选中的，就进行相关的计算操作
     * 3.给textView填充数据
     */
    private void calulate() {
        mtotalPrice = 0.00;
        mtotalCount = 0;
        for (int i = 0; i < groups.size(); i++) {
            StoreInfo group = groups.get(i);
            List<GoodsInfo> child = childs.get(group.getId());
            for (int j = 0; j < child.size(); j++) {
                GoodsInfo good = child.get(j);
                if (good.isChoosed()) {
                    mtotalCount++;
                    mtotalPrice += good.getPrice() * good.getCount();
                }
            }
        }
        totalPrice.setText("￥" + mtotalPrice + "");
        goPay.setText("去支付(" + mtotalCount + ")");
        if (mtotalCount == 0) {
            setCartNum();
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter = null;
        childs.clear();
        groups.clear();
        mtotalPrice = 0.00;
        mtotalCount = 0;
    }


    private String dataJsonStr() {
        return "{\n" +
                "\t\"data\": [{\n" +
                "\t\t\"shopName\": \"百果香水果\",\n" +
                "\t\t\"shopId\": 770,\n" +
                "\t\t\"goodsOrderList\": [{\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-ARuAeqOJAABA86hhvFs753.png\",\n" +
                "\t\t\t\"orderTime\": 1543406612000,\n" +
                "\t\t\t\"orderId\": 1,\n" +
                "\t\t\t\"goodsId\": 15,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 200.00,\n" +
                "\t\t\t\"goodsName\": \"测试商品\",\n" +
                "\t\t\t\"goodsNum\": 2\n" +
                "\t\t}, {\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-AaWAMwndAAJXTYz0D10386.png\",\n" +
                "\t\t\t\"orderTime\": 1543406653000,\n" +
                "\t\t\t\"orderId\": 2,\n" +
                "\t\t\t\"goodsId\": 16,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 100.00,\n" +
                "\t\t\t\"goodsName\": \"微商测试\",\n" +
                "\t\t\t\"goodsNum\": 1\n" +
                "\t\t}, {\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-B5yAP6mEAACAS8Az01I176.png\",\n" +
                "\t\t\t\"orderTime\": 1543406667000,\n" +
                "\t\t\t\"orderId\": 3,\n" +
                "\t\t\t\"goodsId\": 17,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 2000.00,\n" +
                "\t\t\t\"goodsName\": \"百果园水果测试\",\n" +
                "\t\t\t\"goodsNum\": 2\n" +
                "\t\t}]\n" +
                "\t}, {\n" +
                "\t\t\"shopName\": \"水果店\",\n" +
                "\t\t\"shopId\": 779,\n" +
                "\t\t\"goodsOrderList\": [{\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/E8/rBKmn1v89TWAPgzxAABRNNOrEOc793.jpg\",\n" +
                "\t\t\t\"orderTime\": 1543408386000,\n" +
                "\t\t\t\"orderId\": 4,\n" +
                "\t\t\t\"goodsId\": 8,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 48.00,\n" +
                "\t\t\t\"goodsName\": \"车厘子\",\n" +
                "\t\t\t\"goodsNum\": 1\n" +
                "\t\t}, {\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/EB/rBKmn1v9KXOAMLYiAAD5SG2yhBY104.jpg\",\n" +
                "\t\t\t\"orderTime\": 1543408402000,\n" +
                "\t\t\t\"orderId\": 5,\n" +
                "\t\t\t\"goodsId\": 9,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 5.00,\n" +
                "\t\t\t\"goodsName\": \"橘子\",\n" +
                "\t\t\t\"goodsNum\": 1\n" +
                "\t\t}, {\n" +
                "\t\t\t\"goodsImg\": \"http://112.74.187.103:8001/group1/M00/00/EB/rBKmn1v9KZKAUvToAACh_dDD-Xs641.jpg\",\n" +
                "\t\t\t\"orderTime\": 1543408414000,\n" +
                "\t\t\t\"orderId\": 6,\n" +
                "\t\t\t\"goodsId\": 10,\n" +
                "\t\t\t\"integral\": 0,\n" +
                "\t\t\t\"goodsAmount\": 6.00,\n" +
                "\t\t\t\"goodsName\": \"苹果\",\n" +
                "\t\t\t\"goodsNum\": 1\n" +
                "\t\t}]\n" +
                "\t}]\n" +
                "}";
    }

}
