package com.demo.ismyapplication2.entity;

import java.util.List;

/**
 * All rights Reserved, Designed By JiXiangMei Company
 *
 * @version V1.0
 * @Package: com.demo.ismyapplication2.entity
 * @Description:data  全部数据  数据提供用
 * @author: WamHui
 * @date: 2018/11/30
 */
public class ShopCarEntity {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * shopName : 百果香水果
         * shopId : 770
         * goodsOrderList : [{"goodsImg":"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-ARuAeqOJAABA86hhvFs753.png","orderTime":1543406612000,"orderId":1,"goodsId":15,"integral":0,"goodsAmount":200,"goodsName":"测试商品","goodsNum":2},{"goodsImg":"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-AaWAMwndAAJXTYz0D10386.png","orderTime":1543406653000,"orderId":2,"goodsId":16,"integral":0,"goodsAmount":100,"goodsName":"微商测试","goodsNum":1},{"goodsImg":"http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-B5yAP6mEAACAS8Az01I176.png","orderTime":1543406667000,"orderId":3,"goodsId":17,"integral":0,"goodsAmount":2000,"goodsName":"百果园水果测试","goodsNum":2}]
         */

        private String shopName;
        private int shopId;
        private List<GoodsOrderListBean> goodsOrderList;

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public int getShopId() {
            return shopId;
        }

        public void setShopId(int shopId) {
            this.shopId = shopId;
        }

        public List<GoodsOrderListBean> getGoodsOrderList() {
            return goodsOrderList;
        }

        public void setGoodsOrderList(List<GoodsOrderListBean> goodsOrderList) {
            this.goodsOrderList = goodsOrderList;
        }

        public static class GoodsOrderListBean {
            /**
             * goodsImg : http://112.74.187.103:8001/group1/M00/00/EF/rBKmn1v-ARuAeqOJAABA86hhvFs753.png
             * orderTime : 1543406612000
             * orderId : 1
             * goodsId : 15
             * integral : 0
             * goodsAmount : 200.0
             * goodsName : 测试商品
             * goodsNum : 2
             */

            private String goodsImg;
            private long orderTime;
            private int orderId;
            private int goodsId;
            private int integral;
            private double goodsAmount;
            private String goodsName;
            private int goodsNum;

            public String getGoodsImg() {
                return goodsImg;
            }

            public void setGoodsImg(String goodsImg) {
                this.goodsImg = goodsImg;
            }

            public long getOrderTime() {
                return orderTime;
            }

            public void setOrderTime(long orderTime) {
                this.orderTime = orderTime;
            }

            public int getOrderId() {
                return orderId;
            }

            public void setOrderId(int orderId) {
                this.orderId = orderId;
            }

            public int getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(int goodsId) {
                this.goodsId = goodsId;
            }

            public int getIntegral() {
                return integral;
            }

            public void setIntegral(int integral) {
                this.integral = integral;
            }

            public double getGoodsAmount() {
                return goodsAmount;
            }

            public void setGoodsAmount(double goodsAmount) {
                this.goodsAmount = goodsAmount;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public int getGoodsNum() {
                return goodsNum;
            }

            public void setGoodsNum(int goodsNum) {
                this.goodsNum = goodsNum;
            }
        }
    }
}
